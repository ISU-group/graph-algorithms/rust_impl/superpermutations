use graph;

use crate::pheromone::{self, PheromoneType};

pub type DistanceType = f64;

#[derive(Debug)]
pub struct Edge {
    pub distance: DistanceType,
    pub weight: PheromoneType,
    epoch_weight: PheromoneType,
}

impl Edge {
    pub fn add_epoch_weight(&mut self, weight: PheromoneType) {
        self.epoch_weight += weight;
    }

    pub fn pop_epoch_weight(&mut self) -> PheromoneType {
        let weight = self.epoch_weight;
        self.epoch_weight = 0.0;

        weight
    }

    pub fn set_weight(&mut self, weight: PheromoneType) {
        self.weight = weight;
    }
}

impl graph::edge::EdgeTrait for Edge {
    type Weight = PheromoneType;

    fn new(weight: Self::Weight) -> Self {
        Self {
            weight,
            distance: Default::default(),
            epoch_weight: Default::default(),
        }
    }

    fn weight(&self) -> Self::Weight {
        self.weight
    }
}

impl Default for Edge {
    fn default() -> Self {
        Self {
            weight: pheromone::DEFAULT_AMOUNT,
            distance: Default::default(),
            epoch_weight: Default::default(),
        }
    }
}
