use std::collections::HashSet;

use graph::node::NodeId;

use super::edge::DistanceType;

#[derive(Debug, Clone)]
pub(crate) enum AntKind {
    Worker,
    Elite,
}

#[derive(Debug)]
pub(crate) struct Ant {
    node: NodeId,
    kind: AntKind,
    visited: HashSet<NodeId>,
    walked_distance: DistanceType,
    pub(crate) path: Vec<NodeId>,
}
impl Ant {
    pub fn elite(start_node: NodeId) -> Self {
        let mut ant = Self::worker(start_node);
        ant.kind = AntKind::Elite;
        ant
    }

    pub fn worker(start_node: NodeId) -> Self {
        Self {
            kind: AntKind::Worker,
            node: start_node,
            visited: HashSet::from([start_node]),
            walked_distance: Default::default(),
            path: Vec::from([start_node]),
        }
    }

    pub fn visit(&mut self, node: NodeId, distance: DistanceType) {
        self.node = node;
        self.visited.insert(node);
        self.walked_distance += distance;
        self.path.push(node);
    }

    pub fn node(&self) -> NodeId {
        self.node
    }

    pub fn has_visited(&self, node: NodeId) -> bool {
        self.visited.contains(&node)
    }

    pub fn walked_distance(&self) -> DistanceType {
        self.walked_distance
    }

    pub fn kind(&self) -> AntKind {
        self.kind.clone()
    }
}
