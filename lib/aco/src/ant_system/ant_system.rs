use std::ops::AddAssign;

use graph::{
    edge::EdgeTrait,
    graph::Graph,
    node::{BaseNode, NodeId},
};
use rand::seq::IteratorRandom;

use crate::{
    params::{AntPopulation, HeuristicParams},
    pheromone::PheromoneType,
};

use super::{
    ant::Ant,
    edge::{DistanceType, Edge},
    node::Node,
    path::Path,
};

pub type AntGraph = Graph<Node, Edge>;

#[derive(Debug)]
pub struct AntSystem {
    heuristic: HeuristicParams,
    population: AntPopulation,
}

impl AntSystem {
    pub fn new(heuristic: HeuristicParams, population: AntPopulation) -> Self {
        Self {
            heuristic,
            population,
        }
    }

    pub fn optimal_path(&self, graph: &mut AntGraph) -> Path {
        self.search_path(graph)
    }

    fn search_path(&self, graph: &mut AntGraph) -> Path {
        let mut finished_ants = 0;
        let mut ants = Vec::<Ant>::with_capacity(self.population.total());
        self.initialize_ants(&graph, &mut ants);

        let mut path = Path::default();
        path.total_distance = DistanceType::MAX;

        while finished_ants != self.population.total() {
            finished_ants = 0;

            for ant in ants.iter_mut() {
                match self.next_node(&graph, ant) {
                    Some(node) => {
                        let edge = graph.edge_mut_by_ids(ant.node(), node).unwrap();
                        ant.visit(node, edge.distance);

                        edge.add_epoch_weight(self.heuristic.secretion / ant.walked_distance());

                        if ant.path.len() == graph.nodes_number()
                            && ant.walked_distance() < path.total_distance
                            && ant.walked_distance() > 0 as DistanceType
                        {
                            path.total_distance = ant.walked_distance();
                            path.nodes = ant.path.clone();
                        }
                    }
                    None => finished_ants += 1,
                }
            }

            self.update_pheromone(graph);
        }

        path
    }

    fn initialize_ants(&self, graph: &AntGraph, ants: &mut Vec<Ant>) {
        let mut rng = rand::thread_rng();

        for _ in 0..self.population.workers {
            let start_node = graph.nodes().choose(&mut rng).unwrap().id();
            ants.push(Ant::worker(start_node));
        }

        for _ in 0..self.population.elite {
            let start_node = graph.nodes().choose(&mut rng).unwrap().id();
            ants.push(Ant::elite(start_node));
        }
    }

    fn next_node(&self, graph: &AntGraph, ant: &Ant) -> Option<NodeId> {
        let probabilities = self.get_ant_probabilities(graph, ant);

        if probabilities.is_empty() {
            return None;
        }

        match ant.kind() {
            super::ant::AntKind::Worker => {
                let choice = rand::random::<f64>();
                let mut accumulated_prob = 0.0;

                for (node_id, prob) in &probabilities {
                    accumulated_prob.add_assign(prob);

                    if choice < accumulated_prob {
                        return Some(*node_id);
                    }
                }

                Some(probabilities.last().unwrap().0)
            }
            super::ant::AntKind::Elite => {
                let (node, _) = probabilities.iter().max_by(|a, b| a.1.total_cmp(&b.1))?;
                Some(*node)
            }
        }
    }

    fn get_ant_probabilities(&self, graph: &AntGraph, ant: &Ant) -> Vec<(NodeId, f64)> {
        let mut probabilities = graph
            .neighbors_by_id(ant.node())
            .filter_map(|(k, v)| {
                if !ant.has_visited(k.id()) {
                    return Some((k.id(), self.ant_probability(v.weight(), v.distance)));
                }
                None
            })
            .collect::<Vec<_>>();

        let total_probability = probabilities.iter().map(|(_, p)| p).sum::<f64>();
        probabilities
            .iter_mut()
            .for_each(|(_, p)| *p /= total_probability);

        probabilities
    }

    fn ant_probability(&self, pheromone: PheromoneType, distance: DistanceType) -> f64 {
        pheromone.powf(self.heuristic.importance) / distance.powf(self.heuristic.sensitivity)
    }

    fn update_pheromone(&self, graph: &mut AntGraph) {
        for (_, edge) in graph.edges_mut() {
            let weight =
                (1.0 - self.heuristic.evaporation_rate) * edge.weight() + edge.pop_epoch_weight();
            edge.set_weight(weight);
        }
    }
}
