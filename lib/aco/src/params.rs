use crate::pheromone::PheromoneType;

#[derive(Debug)]
pub struct HeuristicParams {
    pub secretion: PheromoneType,
    pub evaporation_rate: PheromoneType,
    pub importance: PheromoneType,
    pub sensitivity: PheromoneType,
}

#[derive(Debug)]
pub struct AntPopulation {
    pub workers: usize,
    pub elite: usize,
}

impl AntPopulation {
    pub fn total(&self) -> usize {
        self.workers + self.elite
    }
}
