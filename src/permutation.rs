use aco::{
    ant_system::{
        edge::DistanceType,
        node::{BaseNode, NamedNodeTrait, NodeId},
    },
    pheromone, AntGraph,
};

#[derive(Debug)]
pub struct Permutation {
    alphabet: Vec<String>,
}

impl Permutation {
    pub fn new(alphabet: &[String]) -> Self {
        Self {
            alphabet: alphabet.to_vec(),
        }
    }

    pub fn graph(&mut self) -> AntGraph {
        let mut graph = AntGraph::default();

        self.add_nodes(&mut graph);
        self.add_edges(&mut graph);

        graph
    }

    fn add_nodes(&mut self, graph: &mut AntGraph) {
        let mut indices = Vec::with_capacity(self.alphabet.len());
        indices.resize(self.alphabet.len(), 0);

        self.add_node(graph);

        let mut i = 1;
        while i < self.alphabet.len() {
            if indices[i] < i {
                let k = if is_odd(i) { indices[i] } else { 0 };

                self.alphabet.swap(i, k);
                self.add_node(graph);

                indices[i] += 1;
                i = 1;
            } else {
                indices[i] = 0;
                i += 1;
            }
        }
    }

    fn add_node(&self, graph: &mut AntGraph) {
        graph
            .add_node(&self.alphabet.join(""))
            .expect("Graph must be empty");
    }

    fn add_edges(&mut self, graph: &mut AntGraph) {
        let node_ids = graph.nodes().map(|node| node.id()).collect::<Vec<_>>();

        for &from_id in &node_ids {
            for &to_id in &node_ids {
                if from_id == to_id {
                    continue;
                }

                let distance = self.get_nodes_distance(from_id, to_id, graph);

                if distance == self.alphabet.len() {
                    continue;
                }

                graph
                    .add_edge_by_ids(from_id, to_id, pheromone::DEFAULT_AMOUNT)
                    .expect("Graph is supposed to contain only nodes before adding edges");
                graph.edge_mut_by_ids(from_id, to_id).unwrap().distance = distance as DistanceType;
            }
        }
    }

    fn get_nodes_distance(&self, from: NodeId, to: NodeId, graph: &AntGraph) -> usize {
        let from_name = graph.node_by_id(from).unwrap().name();
        let to_name = graph.node_by_id(to).unwrap().name();

        for i in 0..from_name.len() {
            if to_name.starts_with(&from_name[i..]) {
                return i;
            }
        }

        self.alphabet.len()
    }
}

fn is_odd(number: usize) -> bool {
    number & 1 == 1
}
