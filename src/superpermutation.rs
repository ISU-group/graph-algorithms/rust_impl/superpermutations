use aco::ant_system::{node::NamedNodeTrait, path::Path, AntGraph};

pub fn construct_superpermutation(path: &Path, graph: &AntGraph) -> String {
    if path.nodes.is_empty() {
        return String::default();
    }

    let mut superpermutation = Vec::default();

    let mut prev_name = graph.node_by_id(path.nodes[0]).unwrap().name();
    for idx in 1..path.nodes.len() {
        let name = graph.node_by_id(path.nodes[idx]).unwrap().name();
        let distance = name_distance(prev_name, name);

        superpermutation.push(&prev_name[0..distance]);
        prev_name = name;
    }
    superpermutation.push(prev_name);

    superpermutation.join("")
}

fn name_distance(lhs: &str, rhs: &str) -> usize {
    for i in 0..lhs.len() {
        if rhs.starts_with(&lhs[i..]) {
            return i;
        }
    }

    lhs.len()
}
