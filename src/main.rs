use aco::{
    params::{AntPopulation, HeuristicParams},
    AntSystem,
};
use clap::Parser;

use crate::permutation::Permutation;
use crate::superpermutation::construct_superpermutation;

mod permutation;
mod superpermutation;

#[derive(Parser)]
struct Cli {
    alphabet_size: usize,
}

fn main() {
    let args = Cli::parse();

    let alphabet = get_alphabet(args.alphabet_size);
    let mut permutation = Permutation::new(&alphabet);

    let mut graph = permutation.graph();

    let heuristic = HeuristicParams {
        secretion: 4.0,
        evaporation_rate: 0.4,
        importance: 8.0,
        sensitivity: 6.0,
    };
    let population = AntPopulation {
        workers: 200,
        elite: 40,
    };

    let ant_system = AntSystem::new(heuristic, population);

    let path = ant_system.optimal_path(&mut graph);
    let superpermutation = construct_superpermutation(&path, &graph);

    println!("{superpermutation}");
    println!("Length: {}", superpermutation.len());
}

fn get_alphabet(alphabet_size: usize) -> Vec<String> {
    let mut alphabet = Vec::with_capacity(alphabet_size);

    for i in 1..=alphabet_size {
        alphabet.push(i.to_string());
    }

    alphabet
}
