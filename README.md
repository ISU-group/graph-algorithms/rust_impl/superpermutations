# Superpermutations
The ant colony optimization algorithm is used to find the optimal path.

Help:
```shell
$ cargo run -r -- --help

Usage: superpermutations <ALPHABET_SIZE>

Arguments:
  <ALPHABET_SIZE>  

Options:
  -h, --help  Print help information
```

Example:
```shell
$ cargo run -r -- 4

341234132413421343124314231432143
Length: 33
```